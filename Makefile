migrate:
	docker-compose run --rm ejemplo_blt ./manage.py makemigrations --settings=ejemplo_blt.settings.dev
	docker-compose run --rm ejemplo_blt ./manage.py migrate auth --settings=ejemplo_blt.settings.dev
	docker-compose run --rm ejemplo_blt ./manage.py migrate --settings=ejemplo_blt.settings.dev

requirements:
	docker-compose run --rm ejemplo_blt pip install -r ejemplo_blt/requirements.txt

statics:
	docker-compose run --rm ejemplo_blt ./manage.py collectstatic --no-input --settings=ejemplo_blt.settings.dev

superuser:
	docker-compose run --rm ejemplo_blt ./manage.py createsuperuser --settings=ejemplo_blt.settings.dev

clean:
	rm -rf ejemplo_blt/*/migrations/00**.py
	find . -name "*.pyc" -exec rm -rf  -- {} +
	find . -name "*__pycache__" -exec rm -rf  -- {} +

test:
	docker-compose run --rm ejemplo_blt ./manage.py test --settings=ejemplo_blt.settings.dev

run:
	docker-compose run --rm ejemplo_blt ./manage.py runserver 0.0.0.0:8000 --settings=ejemplo_blt.settings.dev

delete-all:
	docker-compose down -v
	sudo rm -rf .pgdata/

